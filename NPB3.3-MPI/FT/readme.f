c 148			   call fft(1, u1, u0) -->(1, x1, x2)
						 	call cfftbs1(1, dims(1,1), dims(2,1), dims(3,1), 
     >                  x1, x1, scratch)
              call cffts2(1, dims(1,2), dims(2,2), dims(3,2), 
     >                  x1, x1, scratch)
              call cffts3(1, dims(1,3), dims(2,3), dims(3,3), 
     >                  x1, x2, scratch)

												fft(dir, x1, x2)


c    #of np and problem size will lead to different codes.
c						ft.S.1 => 0D 
c           dim(1,1) = dim(2,1) = dim(3,1) = 64
c 					dims(1,3), dims(2,3), dims(3,3) = 64
c 					twiddle = some fixed index map = 64*64*64
c 					scratch = some temp variables
c 		do iter = 1, niter
c	167		call evolve(u0, u1, twiddle, dims(1,1), dims(2,1), dims(3,1)){
c					u0 <- u0*twiddle  
c  				u1 <- u0  //Need to store "twiddle" and other aux data ?   
c   								//=> application level checkpoint before main loop. 
c				}
c	171		call fft(-1, u1, u2) {
c					call cffts3(-1, 64,64,64, x1, x1, scratch)
c         call cffts2(-1, dims(1,2), dims(2,2), dims(3,2), 
c    >                  x1, x1, scratch)
c         call cffts1(-1, dims(1,1), dims(2,1), dims(3,1), 
c    >                  x1, x2, scratch)
c				}
c				call checksum(iter, u2, dims(1,1), dims(2,1), dims(3,1)) {}
c      enddo









	call cffts1(-1, dims(1,3), dims(2,3), dims(3,3),x1, x1, scratch)
    call transpose_x_yz(3, 2, x1, x2)
            if (timers_enabled) call timer_stop(T_transpose)
            call cffts2(-1, dims(1,2), dims(2,2), dims(3,2), 
     >                  x2, x2, scratch)
            call cffts1(-1, dims(1,1), dims(2,1), dims(3,1), 
     >                  x2, x2, scratch)


            call cffts1(-1, dims(1,3), dims(2,3), dims(3,3), 
     >                  x1, x1, scratch)
            if (timers_enabled) call timer_start(T_transpose)
            call transpose_x_yz(3, 2, x1, x2)
            if (timers_enabled) call timer_stop(T_transpose)
            call cffts2(-1, dims(1,2), dims(2,2), dims(3,2), 
     >                  x2, x2, scratch)
            call cffts1(-1, dims(1,1), dims(2,1), dims(3,1), 
     >                  x2, x2, scratch)