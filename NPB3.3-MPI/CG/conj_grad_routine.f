     subroutine conj_grad_u ( colidx,
     >                       rowstr,
     >                       x,
     >                       z,
     >                       a,
     >                       p,
     >                       q,
     >                       r,
     >                       w,
     >                       rnorm, 
     >                       l2npcols,
     >                       reduce_exch_proc,
     >                       reduce_send_starts,
     >                       reduce_send_lengths,
     >                       reduce_recv_starts,
     >                       reduce_recv_lengths )


      if (timeron) call timer_start(t_conjg)
c---------------------------------------------------------------------
c  Initialize the CG algorithm:
c---------------------------------------------------------------------
c-- naa = 1400, nprows=1
      do j=1,naa/nprows+1
         q(j) = 0.0d0
         z(j) = 0.0d0
         r(j) = x(j)
         p(j) = r(j)
         w(j) = 0.0d0                 
      enddo


c---------------------------------------------------------------------
c  rho = r.r
c  Now, obtain the norm of r: First, sum squares of r elements locally...
c---------------------------------------------------------------------
      sum = 0.0d0
      do j=1, lastcol-firstcol+1
         sum = sum + r(j)*r(j)
      enddo

c---------------------------------------------------------------------
c  Exchange and sum with procs identified in reduce_exch_proc
c  (This is equivalent to mpi_allreduce.)
c  Sum the partial sums of rho, leaving rho on all processors
c---------------------------------------------------------------------
      do i = 1, l2npcols
         if (timeron) call timer_start(t_rcomm)
         call mpi_irecv( rho,
     >                   1,
     >                   dp_type,
     >                   reduce_exch_proc(i),
     >                   i,
     >                   mpi_comm_world,
     >                   request,
     >                   ierr )
         call mpi_send(  sum,
     >                   1,
     >                   dp_type,
     >                   reduce_exch_proc(i),
     >                   i,
     >                   mpi_comm_world,
     >                   ierr )
         call mpi_wait( request, status, ierr )
         if (timeron) call timer_stop(t_rcomm)

         sum = sum + rho
      enddo
      rho = sum



c---------------------------------------------------------------------
c---->
c  The conj grad iteration loop
c---->
c---------------------------------------------------------------------
c         cgitmax =25  lastrow = 1400 firstrow= 1
      do cgit = 1, cgitmax


c---------------------------------------------------------------------
c  q = A.p
c  The partition submatrix-vector multiply: use workspace w
c---------------------------------------------------------------------
c--ychuang     p=>read only, index based
         do j=1,lastrow-firstrow+1
            sum = 0.d0
            do k=rowstr(j),rowstr(j+1)-1
               sum = sum + a(k)*p(colidx(k))
            enddo
            w(j) = sum
         enddo

c---------------------------------------------------------------------
c  Sum the partition submatrix-vec A.p's across rows
c  Exchange and sum piece of w with procs identified in reduce_exch_proc
c---------------------------------------------------------------------
         do i = l2npcols, 1, -1
            if (timeron) call timer_start(t_rcomm)
            call mpi_irecv( q(reduce_recv_starts(i)),
     >                      reduce_recv_lengths(i),
     >                      dp_type,
     >                      reduce_exch_proc(i),
     >                      i,
     >                      mpi_comm_world,
     >                      request,
     >                      ierr )
            call mpi_send(  w(reduce_send_starts(i)),
     >                      reduce_send_lengths(i),
     >                      dp_type,
     >                      reduce_exch_proc(i),
     >                      i,
     >                      mpi_comm_world,
     >                      ierr )
            call mpi_wait( request, status, ierr )
            if (timeron) call timer_stop(t_rcomm)
            do j=send_start,send_start + reduce_recv_lengths(i) - 1
               w(j) = w(j) + q(j)
            enddo
         enddo
      

c---------------------------------------------------------------------
c  Exchange piece of q with transpose processor:
c---------------------------------------------------------------------
         if( l2npcols .ne. 0 )then
            if (timeron) call timer_start(t_rcomm)
            call mpi_irecv( q,               
     >                      exch_recv_length,
     >                      dp_type,
     >                      exch_proc,
     >                      1,
     >                      mpi_comm_world,
     >                      request,
     >                      ierr )

            call mpi_send(  w(send_start),   
     >                      send_len,
     >                      dp_type,
     >                      exch_proc,
     >                      1,
     >                      mpi_comm_world,
     >                      ierr )
            call mpi_wait( request, status, ierr )
            if (timeron) call timer_stop(t_rcomm)
         else
            do j=1,exch_recv_length
               q(j) = w(j)
            enddo
         endif


c---------------------------------------------------------------------
c  Clear w for reuse...
c---------------------------------------------------------------------
         do j=1, max( lastrow-firstrow+1, lastcol-firstcol+1 )
            w(j) = 0.0d0
         enddo
         

c---------------------------------------------------------------------
c  Obtain p.q
c---------------------------------------------------------------------
         sum = 0.0d0
         do j=1, lastcol-firstcol+1
            sum = sum + p(j)*q(j)
         enddo

c---------------------------------------------------------------------
c  Obtain d with a sum-reduce
c---------------------------------------------------------------------
         do i = 1, l2npcols
            if (timeron) call timer_start(t_rcomm)
            call mpi_irecv( d,
     >                      1,
     >                      dp_type,
     >                      reduce_exch_proc(i),
     >                      i,
     >                      mpi_comm_world,
     >                      request,
     >                      ierr )
            call mpi_send(  sum,
     >                      1,
     >                      dp_type,
     >                      reduce_exch_proc(i),
     >                      i,
     >                      mpi_comm_world,
     >                      ierr )

            call mpi_wait( request, status, ierr )
            if (timeron) call timer_stop(t_rcomm)

            sum = sum + d
         enddo
         d = sum


c---------------------------------------------------------------------
c  Obtain alpha = rho / (p.q)
c---------------------------------------------------------------------
         alpha = rho / d

c---------------------------------------------------------------------
c  Save a temporary of rho
c---------------------------------------------------------------------
         rho0 = rho

c---------------------------------------------------------------------
c  Obtain z = z + alpha*p
c  and    r = r - alpha*q
c---------------------------------------------------------------------
c-- lastcol = 1400 firstcol = 1
c-- update all z, update all r
         do j=1, lastcol-firstcol+1
            z(j) = z(j) + alpha*p(j)
            r(j) = r(j) - alpha*q(j)
         enddo
            
c---------------------------------------------------------------------
c  rho = r.r
c  Now, obtain the norm of r: First, sum squares of r elements locally...
c---------------------------------------------------------------------
         sum = 0.0d0
         do j=1, lastcol-firstcol+1
            sum = sum + r(j)*r(j)
         enddo

c---------------------------------------------------------------------
c  Obtain rho with a sum-reduce
c---------------------------------------------------------------------
         do i = 1, l2npcols
            if (timeron) call timer_start(t_rcomm)
            call mpi_irecv( rho,
     >                      1,
     >                      dp_type,
     >                      reduce_exch_proc(i),
     >                      i,
     >                      mpi_comm_world,
     >                      request,
     >                      ierr )
            call mpi_send(  sum,
     >                      1,
     >                      dp_type,
     >                      reduce_exch_proc(i),
     >                      i,
     >                      mpi_comm_world,
     >                      ierr )
            call mpi_wait( request, status, ierr )
            if (timeron) call timer_stop(t_rcomm)

            sum = sum + rho
         enddo
         rho = sum

c---------------------------------------------------------------------
c  Obtain beta:
c---------------------------------------------------------------------
         beta = rho / rho0

c---------------------------------------------------------------------
c  p = r + beta*p
c---------------------------------------------------------------------
c--ychuang
c       update all p
         do j=1, lastcol-firstcol+1
            p(j) = r(j) + beta*p(j)
         enddo



      enddo                             ! end of do cgit=1,cgitmax