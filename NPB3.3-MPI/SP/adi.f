
c---------------------------------------------------------------------
c---------------------------------------------------------------------

       subroutine  adi(u_new, u_old)

c---------------------------------------------------------------------
c---------------------------------------------------------------------

       call copy_faces(u_old)

       call txinvr

       call x_solve(u_old)

       call y_solve(u_old)

       call z_solve(u_old)

       call add(u_new, u_old)

       return
       end

