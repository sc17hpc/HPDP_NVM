c----------------------------------------------------------------------
      subroutine kw_init
      include 'SIZE'
      include 'TOTAL'
      include 'DOMAIN'
      include 'ZPER'

      include 'OPCTR'
      include 'CTIMER'

      integer data_pos
c-----use mylib/mylib.c
c---- call unimem_malloc(ptr_a, sizeof(a), data_pos)

c-----------control data palcement   -1 NVM DRAM 1
      data_pos = -1
  
c----------------------SIZE-----------------------
      call unimem_malloc(ptr_ldim, ldim, data_pos)
      call unimem_malloc(ptr_lx1, lx1, data_pos)
      call unimem_malloc(ptr_lxd, lxd, data_pos)
      call unimem_malloc(ptr_lx2, lx2, data_pos)
      call unimem_malloc(ptr_lx1m, lx1m, data_pos)
      call unimem_malloc(ptr_lelg, lelg, data_pos)
      call unimem_malloc(ptr_lp, lp, data_pos)
      call unimem_malloc(ptr_lelt, lelt, data_pos)
      call unimem_malloc(ptr_ldimt, ldimt, data_pos)

      call unimem_malloc(ptr_ax1, ax1, data_pos)
      call unimem_malloc(ptr_ax2, ax2, data_pos)
      call unimem_malloc(ptr_lpx1, lpx1, data_pos)
      call unimem_malloc(ptr_lpx2, lpx2, data_pos)
      call unimem_malloc(ptr_lpelt, lpelt, data_pos)
      call unimem_malloc(ptr_lbx1, lbx1, data_pos)
      call unimem_malloc(ptr_lbx2, lbx2, data_pos)
      call unimem_malloc(ptr_lhis, lhis, data_pos)

      call unimem_malloc(ptr_lcvelt, lcvelt, data_pos)
      call unimem_malloc(ptr_lcvx1, lcvx1, data_pos)
      call unimem_malloc(ptr_toteq, toteq, data_pos)
      call unimem_malloc(ptr_lelcmt, lelcmt, data_pos)

      call unimem_malloc(ptr_lelx, lelx, data_pos)
      call unimem_malloc(ptr_lely, lely, data_pos)
      call unimem_malloc(ptr_lelz, lelz, data_pos)
      call unimem_malloc(ptr_mxprev, mxprev, data_pos)
      call unimem_malloc(ptr_lgmres, lgmres, data_pos)
      call unimem_malloc(ptr_lorder, lorder, data_pos)
      call unimem_malloc(ptr_lhis, lhis, data_pos)

      call unimem_malloc(ptr_maxobj, maxobj, data_pos)
      call unimem_malloc(ptr_maxmbr, maxmbr, data_pos)
      call unimem_malloc(ptr_lpert, lpert, data_pos)
      call unimem_malloc(ptr_nsessmax, nsessmax, data_pos)
      call unimem_malloc(ptr_nmaxl, nmaxl, data_pos)
      call unimem_malloc(ptr_nfldmax, nfldmax, data_pos)
      call unimem_malloc(ptr_nmaxcom, nmaxcom, data_pos)

C------------------TOTAL--------------------------------------
c------------------DXYZ---------------------------------------
      call unimem_malloc(ptr_dxm1, sizeof(dxm1), data_pos)
      call unimem_malloc(ptr_dxm12, sizeof(dxm12), data_pos)
      call unimem_malloc(ptr_dym1, sizeof(dym1), data_pos)
      call unimem_malloc(ptr_dym12, sizeof(dym12), data_pos)
      call unimem_malloc(ptr_dzm1, sizeof(dzm1), data_pos)
      call unimem_malloc(ptr_dzm12, sizeof(dzm12), data_pos)
      call unimem_malloc(ptr_dxtm1, sizeof(dxtm1), data_pos)
      call unimem_malloc(ptr_dxtm12, sizeof(dxtm12), data_pos)
      call unimem_malloc(ptr_dytm1, sizeof(dytm1), data_pos)
      call unimem_malloc(ptr_dytm12, sizeof(dytm12), data_pos)
      call unimem_malloc(ptr_dztm1, sizeof(dztm1), data_pos)
      call unimem_malloc(ptr_dztm12, sizeof(dztm12), data_pos)
      call unimem_malloc(ptr_dxm3, sizeof(dxm3), data_pos)
      call unimem_malloc(ptr_dxtm3, sizeof(dxtm3), data_pos)
      call unimem_malloc(ptr_dym3, sizeof(dym3), data_pos)
      call unimem_malloc(ptr_dytm3, sizeof(dytm3), data_pos)
      call unimem_malloc(ptr_dzm3, sizeof(dzm3), data_pos)
      call unimem_malloc(ptr_dztm3, sizeof(dztm3), data_pos)
      call unimem_malloc(ptr_dcm1, sizeof(dcm1), data_pos)
      call unimem_malloc(ptr_dctm1, sizeof(dctm1), data_pos)
      call unimem_malloc(ptr_dcm3, sizeof(dcm3), data_pos)
      call unimem_malloc(ptr_dctm3, sizeof(dctm3), data_pos)
      call unimem_malloc(ptr_dcm12, sizeof(dcm12), data_pos)
      call unimem_malloc(ptr_dctm12, sizeof(dctm12), data_pos)
      call unimem_malloc(ptr_dam1, sizeof(dam1), data_pos)
      call unimem_malloc(ptr_datm1, sizeof(datm1), data_pos)
      call unimem_malloc(ptr_dam12, sizeof(dam12), data_pos)
      call unimem_malloc(ptr_datm12, sizeof(datm12), data_pos)
      call unimem_malloc(ptr_dam3, sizeof(dam3), data_pos)
      call unimem_malloc(ptr_datm3, sizeof(datm3), data_pos)

c---------------------DEALIAS-----------------------------------
      call unimem_malloc(ptr_vxd, sizeof(vxd), data_pos)
      call unimem_malloc(ptr_vyd, sizeof(vyd), data_pos)
      call unimem_malloc(ptr_vzd, sizeof(vzd), data_pos)

      call unimem_malloc(ptr_imd1, sizeof(imd1), data_pos)
      call unimem_malloc(ptr_imd1t, sizeof(imd1t), data_pos)
      call unimem_malloc(ptr_im1d, sizeof(im1d), data_pos)
      call unimem_malloc(ptr_im1dt, sizeof(im1dt), data_pos)
      call unimem_malloc(ptr_pmd1, sizeof(pmd1), data_pos)
      call unimem_malloc(ptr_pmd1t, sizeof(pmd1t), data_pos)

c--------------------EIGEN------------------------                                                                                                
      call unimem_malloc(ptr_eigas,sizeof(eigas), data_pos)
      call unimem_malloc(ptr_eigaa, sizeof(eigaa), data_pos)
      call unimem_malloc(ptr_eigast, sizeof(eigast), data_pos)
      call unimem_malloc(ptr_eigae, sizeof(eigae), data_pos)
      call unimem_malloc(ptr_eigga, sizeof(eigga), data_pos)
      call unimem_malloc(ptr_eiggs, sizeof(eiggs), data_pos)
      call unimem_malloc(ptr_eiggst, sizeof(eiggst), data_pos)
      call unimem_malloc(ptr_eigge, sizeof(eigge), data_pos)

      call unimem_malloc(ptr_ifaa, sizeof(ifaa), data_pos)
      call unimem_malloc(ptr_ifae, sizeof(ifae), data_pos)
      call unimem_malloc(ptr_ifas, sizeof(ifas), data_pos)
      call unimem_malloc(ptr_ifast, sizeof(ifast), data_pos)
      call unimem_malloc(ptr_ifga, sizeof(ifga), data_pos)
      call unimem_malloc(ptr_ifge, sizeof(ifge), data_pos)
      call unimem_malloc(ptr_ifgs, sizeof(ifgs), data_pos)
      call unimem_malloc(ptr_ifgst, sizeof(ifgst), data_pos)

c-----------------------GEOM-----------------------------
      
      call unimem_malloc(ptr_xm1, sizeof(xm1), 1)
      print *, "sizeof(xm1)", sizeof(xm1)

      call unimem_malloc(ptr_ym1, sizeof(ym1), 1)
      print *, "sizeof(ym1)", sizeof(ym1)

      call unimem_malloc(ptr_zm1, sizeof(zm1), 1)
      print *, "sizeof(zm1)", sizeof(ym1)

      call unimem_malloc(ptr_xm1_copy, sizeof(xm1_copy), -1)
      call unimem_malloc(ptr_ym1_copy, sizeof(ym1_copy), -1)
      call unimem_malloc(ptr_zm1_copy, sizeof(zm1_copy), -1)
      call unimem_malloc(ptr_xm1_copy2, sizeof(xm1_copy2), -1)
      call unimem_malloc(ptr_ym1_copy2, sizeof(ym1_copy2), -1)
      call unimem_malloc(ptr_zm1_copy2, sizeof(zm1_copy2), -1)

      call unimem_malloc(ptr_xm2, sizeof(xm2), data_pos)
      call unimem_malloc(ptr_ym2, sizeof(ym2), data_pos)
      call unimem_malloc(ptr_zm2, sizeof(zm2), data_pos)

      call unimem_malloc(ptr_rxm1, sizeof(rxm1), data_pos)
      call unimem_malloc(ptr_sxm1, sizeof(sxm1), data_pos)
      call unimem_malloc(ptr_txm1, sizeof(txm1), data_pos)
      call unimem_malloc(ptr_rym1, sizeof(rym1), data_pos)
      call unimem_malloc(ptr_sym1, sizeof(sym1), data_pos)
      call unimem_malloc(ptr_tym1, sizeof(tym1), data_pos)
      call unimem_malloc(ptr_rzm1, sizeof(rzm1), data_pos)
      call unimem_malloc(ptr_szm1, sizeof(szm1), data_pos)
      call unimem_malloc(ptr_tzm1, sizeof(tzm1), data_pos)
      call unimem_malloc(ptr_jacm1, sizeof(jacm1), data_pos)
      call unimem_malloc(ptr_jacmi, sizeof(jacmi), data_pos)

      call unimem_malloc(ptr_rxm2, sizeof(rxm2), data_pos)
      call unimem_malloc(ptr_sxm2, sizeof(sxm2), data_pos)
      call unimem_malloc(ptr_txm2, sizeof(txm2), data_pos)
      call unimem_malloc(ptr_rym2, sizeof(rym2), data_pos)
      call unimem_malloc(ptr_sym2, sizeof(sym2), data_pos)
      call unimem_malloc(ptr_tym2, sizeof(tym2), data_pos)
      call unimem_malloc(ptr_rzm2, sizeof(rzm2), data_pos)
      call unimem_malloc(ptr_szm2, sizeof(szm2), data_pos)
      call unimem_malloc(ptr_tzm2, sizeof(tzm2), data_pos)
      call unimem_malloc(ptr_jacm2, sizeof(jacm2), data_pos)

      call unimem_malloc(ptr_rx, sizeof(rx), data_pos)

      call unimem_malloc(ptr_g1m1, sizeof(g1m1), data_pos)
      call unimem_malloc(ptr_g2m1, sizeof(g2m1), data_pos)
      call unimem_malloc(ptr_g3m1, sizeof(g3m1), data_pos)
      call unimem_malloc(ptr_g4m1, sizeof(g4m1), data_pos)
      call unimem_malloc(ptr_g5m1, sizeof(g5m1), data_pos)
      call unimem_malloc(ptr_g6m1, sizeof(g6m1), data_pos)

      call unimem_malloc(ptr_unr, sizeof(unr), data_pos)
      call unimem_malloc(ptr_uns, sizeof(uns), data_pos)
      call unimem_malloc(ptr_unt, sizeof(unt), data_pos)
      call unimem_malloc(ptr_unx, sizeof(unx), data_pos)
      call unimem_malloc(ptr_uny, sizeof(uny), data_pos)
      call unimem_malloc(ptr_unz, sizeof(unz), data_pos)
      call unimem_malloc(ptr_t1x, sizeof(t1x), data_pos)
      call unimem_malloc(ptr_t1y, sizeof(t1y), data_pos)
      call unimem_malloc(ptr_t1z, sizeof(t1z), data_pos)
      call unimem_malloc(ptr_t2x, sizeof(t2x), data_pos)
      call unimem_malloc(ptr_t2y, sizeof(t2y), data_pos)
      call unimem_malloc(ptr_t2z, sizeof(t2z), data_pos)
      call unimem_malloc(ptr_area, sizeof(area), data_pos)
      call unimem_malloc(ptr_etalph, sizeof(etalph), data_pos)
      call unimem_malloc(ptr_dlam, sizeof(dlam), data_pos)

      call unimem_malloc(ptr_vnx, sizeof(vnx), data_pos)
      call unimem_malloc(ptr_vny, sizeof(vny), data_pos)
      call unimem_malloc(ptr_vnz, sizeof(vnz), data_pos)
      call unimem_malloc(ptr_v1x, sizeof(v1x), data_pos)
      call unimem_malloc(ptr_v1y, sizeof(v1y), data_pos)
      call unimem_malloc(ptr_v1z, sizeof(v1z), data_pos)
      call unimem_malloc(ptr_v2x, sizeof(v2x), data_pos)
      call unimem_malloc(ptr_v2y, sizeof(v2y), data_pos)
      call unimem_malloc(ptr_v2z, sizeof(v2z), data_pos)

      call unimem_malloc(ptr_ifgeom, sizeof(ifgeom), data_pos)
      call unimem_malloc(ptr_ifgmsh3, sizeof(ifgmsh3), data_pos)
      call unimem_malloc(ptr_ifvcor, sizeof(ifvcor), data_pos)
      call unimem_malloc(ptr_ifsurt, sizeof(ifsurt), data_pos)
      call unimem_malloc(ptr_ifmelt, sizeof(ifmelt), data_pos)
      call unimem_malloc(ptr_ifwcno, sizeof(ifwcno), data_pos)
      call unimem_malloc(ptr_ifrzer, sizeof(ifrzer), data_pos)
      call unimem_malloc(ptr_ifqinp, sizeof(ifqinp), data_pos)
      call unimem_malloc(ptr_ifeppm, sizeof(ifeppm), data_pos)
      call unimem_malloc(ptr_iflmsf, sizeof(iflmsf), data_pos)
      call unimem_malloc(ptr_iflmse, sizeof(iflmse), data_pos)
      call unimem_malloc(ptr_iflmsc, sizeof(iflmsc), data_pos)
      call unimem_malloc(ptr_ifmsfc, sizeof(ifmsfc), data_pos)
      call unimem_malloc(ptr_ifmseg, sizeof(ifmseg), data_pos)
      call unimem_malloc(ptr_ifmscr, sizeof(ifmscr), data_pos)
      call unimem_malloc(ptr_ifnskp, sizeof(ifnskp), data_pos)
      call unimem_malloc(ptr_ifbcor, sizeof(ifbcor), data_pos)

c----------------------INPUT-------------------------------------

      call unimem_malloc(ptr_param, sizeof(param), data_pos)
      call unimem_malloc(ptr_rstim, sizeof(rstim), data_pos)
      call unimem_malloc(ptr_vnekton, sizeof(vnekton), data_pos)
      call unimem_malloc(ptr_cpfld, sizeof(cpfld), data_pos)
      call unimem_malloc(ptr_cpgrp, sizeof(cpgrp), data_pos)
      call unimem_malloc(ptr_qinteg, sizeof(qinteg), data_pos)
      call unimem_malloc(ptr_uparam, sizeof(uparam), data_pos)
      call unimem_malloc(ptr_atol, sizeof(atol), data_pos)

      call unimem_malloc(ptr_matype, sizeof(matype), data_pos)
      call unimem_malloc(ptr_nktonv, sizeof(nktov), data_pos)
      call unimem_malloc(ptr_nhis, sizeof(nhis), data_pos)
      call unimem_malloc(ptr_lochis, sizeof(lochis), data_pos)
      call unimem_malloc(ptr_ipscal, sizeof(ipscal), data_pos)
      call unimem_malloc(ptr_npscal, sizeof(npscal), data_pos)
      call unimem_malloc(ptr_ipsco, sizeof(ipsco), data_pos)
      call unimem_malloc(ptr_ifldmhd, sizeof(ifldmhd), data_pos)
      call unimem_malloc(ptr_irstv, sizeof(irstv), data_pos)
      call unimem_malloc(ptr_irstt, sizeof(irstt), data_pos)
      call unimem_malloc(ptr_irstim, sizeof(irstim), data_pos)
      call unimem_malloc(ptr_nmember, sizeof(nmember), data_pos)
      call unimem_malloc(ptr_nobj, sizeof(nobj), data_pos)
      call unimem_malloc(ptr_ngeom, sizeof(ngeom), data_pos)
      call unimem_malloc(ptr_idpss, sizeof(idpss), data_pos)

       call unimem_malloc(ptr_if3d,sizeof(if3d), data_pos)
       call unimem_malloc(ptr_ifflow,sizeof(ifflow), data_pos)
       call unimem_malloc(ptr_ifheat,sizeof(ifheat), data_pos)
       call unimem_malloc(ptr_iftran,sizeof(iftran), data_pos)
       call unimem_malloc(ptr_ifaxis,sizeof(ifaxis), data_pos)
       call unimem_malloc(ptr_ifstrs,sizeof(ifstrs), data_pos)
       call unimem_malloc(ptr_ifsplit,sizeof(ifsplit), data_pos)
       call unimem_malloc(ptr_ifmgrid,sizeof(ifmgrid), data_pos)
c       call unimem_malloc(ptr_ifadvc,sizeof(ifadvc), data_pos)
       call unimem_malloc(ptr_ifdiff,sizeof(ifdiff), data_pos)
       call unimem_malloc(ptr_ifdeal,sizeof(ifdeal), data_pos)
       call unimem_malloc(ptr_iftmsh,sizeof(iftmsh), data_pos)
       call unimem_malloc(ptr_ifdgfld,sizeof(ifdgfld), data_pos)
       call unimem_malloc(ptr_ifdg,sizeof(ifdg), data_pos)
       call unimem_malloc(ptr_ifmvbd,sizeof(ifmvbd), data_pos)
       call unimem_malloc(ptr_ifnatc,sizeof(ifnatc), data_pos)
       call unimem_malloc(ptr_ifchar,sizeof(ifchar), data_pos)
       call unimem_malloc(ptr_ifnonl,sizeof(ifnonl), data_pos)
       call unimem_malloc(ptr_ifvarp,sizeof(ifvarp), data_pos)
       call unimem_malloc(ptr_ifpsco,sizeof(ifpsco), data_pos)
       call unimem_malloc(ptr_ifvps,sizeof(ifvps), data_pos)
       call unimem_malloc(ptr_ifmodel,sizeof(ifmodel), data_pos)
       call unimem_malloc(ptr_ifkeps,sizeof(ifkeps), data_pos)
       call unimem_malloc(ptr_ifintq,sizeof(ifintq), data_pos)
       call unimem_malloc(ptr_ifcons,sizeof(ifcons), data_pos)
       call unimem_malloc(ptr_ifxyo,sizeof(ifxyo), data_pos)
       call unimem_malloc(ptr_ifpo,sizeof(ifpo), data_pos)
       call unimem_malloc(ptr_ifvo,sizeof(ifvo), data_pos)
       call unimem_malloc(ptr_ifto,sizeof(ifto), data_pos)
       call unimem_malloc(ptr_iftgo,sizeof(iftgo), data_pos)
       call unimem_malloc(ptr_ifpso,sizeof(ifpso), data_pos)
       call unimem_malloc(ptr_iffmtin,sizeof(iffmtin), data_pos)
       call unimem_malloc(ptr_ifbo,sizeof(ifbo), data_pos)
       call unimem_malloc(ptr_ifanls,sizeof(ifanls), data_pos)
       call unimem_malloc(ptr_ifanl2,sizeof(ifanl2), data_pos)
       call unimem_malloc(ptr_ifmhd,sizeof(ifmhd), data_pos)
       call unimem_malloc(ptr_ifessr,sizeof(ifessr), data_pos)
       call unimem_malloc(ptr_ifpert,sizeof(ifpert), data_pos)
       call unimem_malloc(ptr_ifbase,sizeof(ifbase), data_pos)
       call unimem_malloc(ptr_ifcvode,sizeof(ifcvode), data_pos)
       call unimem_malloc(ptr_iflomach,sizeof(iflomach), data_pos)
       call unimem_malloc(ptr_ifexplvis,sizeof(ifexplvis), data_pos)
       call unimem_malloc(ptr_ifschclob,sizeof(ifschclob), data_pos)
       call unimem_malloc(ptr_ifuservp,sizeof(ifuservp), data_pos)
       call unimem_malloc(ptr_ifcyclic,sizeof(ifcyclic), data_pos)
       call unimem_malloc(ptr_ifmoab,sizeof(ifmoab), data_pos)
       call unimem_malloc(ptr_ifcoup,sizeof(ifcoup), data_pos)
       call unimem_malloc(ptr_ifvcoup,sizeof(ifvcoup), data_pos)
       call unimem_malloc(ptr_ifusermv,sizeof(ifusermv), data_pos)
       call unimem_malloc(ptr_ifreguo,sizeof(ifreguo), data_pos)
       call unimem_malloc(ptr_ifxyo_,sizeof(ifxyo_), data_pos)
       call unimem_malloc(ptr_ifaziv,sizeof(ifaziv), data_pos)
       call unimem_malloc(ptr_ifneknek,sizeof(ifneknek), data_pos)
       call unimem_malloc(ptr_ifneknekm,sizeof(ifneknekm), data_pos)
       call unimem_malloc(ptr_ifcvfld,sizeof(ifcvfld), data_pos)
       call unimem_malloc(ptr_ifdp0dt,sizeof(ifdp0dt), data_pos)

       call unimem_malloc(ptr_hcode,sizeof(hcode), data_pos)
       call unimem_malloc(ptr_ocode,sizeof(ocode), data_pos)
c       call unimem_malloc(ptr_drivc,sizeof(drivc), data_pos)
c       call unimem_malloc(ptr_rstv,sizeof(rstv), data_pos)
c       call unimem_malloc(ptr_rstt,sizeof(rstt), data_pos)
c       call unimem_malloc(ptr_textsw,sizeof(textsw), data_pos)
c       call unimem_malloc(ptr_initc,sizeof(initc), data_pos)

       call unimem_malloc(ptr_reafle, sizeof(reafle), data_pos)
       call unimem_malloc(ptr_fldfle, sizeof(fldfle), data_pos)
       call unimem_malloc(ptr_dmpfle, sizeof(dmpfle), data_pos)
       call unimem_malloc(ptr_hisfle, sizeof(hisfle), data_pos)
       call unimem_malloc(ptr_schfle, sizeof(schfle), data_pos)
       call unimem_malloc(ptr_orefle, sizeof(orefle), data_pos)
       call unimem_malloc(ptr_nrefle, sizeof(nrefle), data_pos)

c       call unimem_malloc(ptr_session, sizeof(session), data_pos)
c       call unimem_malloc(ptr_path, sizeof(path), data_pos)
       call unimem_malloc(ptr_re2fle, sizeof(re2fle), data_pos)
       call unimem_malloc(ptr_parfle, sizeof(parfle), data_pos)
       call unimem_malloc(ptr_cr_re2, sizeof(cr_re2), data_pos)
       call unimem_malloc(ptr_fh_re2, sizeof(fh_re2), data_pos)
       call unimem_malloc(ptr_re2off_b, sizeof(re2off_b), data_pos)
       call unimem_malloc(ptr_xc, sizeof(xc), data_pos)
       call unimem_malloc(ptr_yc, sizeof(yc), data_pos)
       call unimem_malloc(ptr_zc, sizeof(zc), data_pos)
       call unimem_malloc(ptr_bc, sizeof(bc), data_pos)
       call unimem_malloc(ptr_curve, sizeof(curve), data_pos)
       call unimem_malloc(ptr_cerror, sizeof(cerror), data_pos)

       call unimem_malloc(ptr_ieact, sizeof(ieact), data_pos)
       call unimem_malloc(ptr_neact, sizeof(neact), data_pos)
       call unimem_malloc(ptr_numsts, sizeof(numsts), data_pos)
       call unimem_malloc(ptr_numflu, sizeof(numflu), data_pos)
       call unimem_malloc(ptr_numoth, sizeof(numoth), data_pos)
       call unimem_malloc(ptr_numbcs, sizeof(numbcs), data_pos)
       call unimem_malloc(ptr_maxindx, sizeof(maxindx), data_pos)
       call unimem_malloc(ptr_matids, sizeof(matids), data_pos)
       call unimem_malloc(ptr_imatie, sizeof(imatie), data_pos)
       call unimem_malloc(ptr_ibcsts, sizeof(ibcsts), data_pos)
       call unimem_malloc(ptr_bcf, sizeof(bcf), data_pos)
       call unimem_malloc(ptr_bctyps, sizeof(bctyps), data_pos)

c--------------------------------IXYZ-------------------------------
      call unimem_malloc(ptr_ixm12,sizeof(ixm12), data_pos)
      call unimem_malloc(ptr_ixm21,sizeof(ixm21), data_pos)
      call unimem_malloc(ptr_iym12,sizeof(iym12), data_pos)
      call unimem_malloc(ptr_iym21,sizeof(iym21), data_pos)
      call unimem_malloc(ptr_izm12,sizeof(izm12), data_pos)
      call unimem_malloc(ptr_izm21,sizeof(izm21), data_pos)
      call unimem_malloc(ptr_ixtm12,sizeof(ixtm12), data_pos)
      call unimem_malloc(ptr_ixtm21,sizeof(ixtm21), data_pos)
      call unimem_malloc(ptr_iytm12,sizeof(iytm12), data_pos)
      call unimem_malloc(ptr_iytm21,sizeof(iytm21), data_pos)
      call unimem_malloc(ptr_iztm12,sizeof(iztm12), data_pos)
      call unimem_malloc(ptr_iztm21,sizeof(iztm21), data_pos)
      call unimem_malloc(ptr_ixm13,sizeof(ixm13), data_pos)
      call unimem_malloc(ptr_ixm31,sizeof(ixm31), data_pos)
      call unimem_malloc(ptr_iym13,sizeof(iym13), data_pos)
      call unimem_malloc(ptr_iym31,sizeof(iym31), data_pos)
      call unimem_malloc(ptr_izm13,sizeof(izm13), data_pos)
      call unimem_malloc(ptr_izm31,sizeof(izm31), data_pos)
      call unimem_malloc(ptr_ixtm13,sizeof(ixtm13), data_pos)
      call unimem_malloc(ptr_ixtm31,sizeof(ixtm31), data_pos)
      call unimem_malloc(ptr_iytm13,sizeof(iytm13), data_pos)
      call unimem_malloc(ptr_iytm31,sizeof(iytm31), data_pos)
      call unimem_malloc(ptr_iztm13,sizeof(iztm13), data_pos)
      call unimem_malloc(ptr_iztm31,sizeof(iztm31), data_pos)

      call unimem_malloc(ptr_iam12, sizeof(iam12), data_pos)
      call unimem_malloc(ptr_iatm12, sizeof(iatm12), data_pos)
      call unimem_malloc(ptr_iam13, sizeof(iam13), data_pos)
      call unimem_malloc(ptr_iatm13, sizeof(iatm13), data_pos)
      call unimem_malloc(ptr_icm12, sizeof(icm12), data_pos)
      call unimem_malloc(ptr_ictm12, sizeof(ictm12), data_pos)
      call unimem_malloc(ptr_icm13, sizeof(icm13), data_pos)
      call unimem_malloc(ptr_ictm13, sizeof(ictm13), data_pos)
      call unimem_malloc(ptr_iajl1, sizeof(iajl1), data_pos)
      call unimem_malloc(ptr_iajl2, sizeof(iajl2), data_pos)
      call unimem_malloc(ptr_ialj3, sizeof(ialj3), data_pos)
      call unimem_malloc(ptr_ialj1, sizeof(ialj1), data_pos)
      call unimem_malloc(ptr_iam21, sizeof(iam21), data_pos)
      call unimem_malloc(ptr_iatm21, sizeof(iatm21), data_pos)
      call unimem_malloc(ptr_iam31, sizeof(iam31), data_pos)
      call unimem_malloc(ptr_iatm31, sizeof(iatm31), data_pos)
      call unimem_malloc(ptr_icm21, sizeof(icm21), data_pos)
      call unimem_malloc(ptr_ictm21, sizeof(ictm21), data_pos)
      call unimem_malloc(ptr_icm31, sizeof(icm31), data_pos)
      call unimem_malloc(ptr_ictm31, sizeof(ictm31), data_pos)
      call unimem_malloc(ptr_iatjl1, sizeof(iatjl1), data_pos)
      call unimem_malloc(ptr_iatjl2, sizeof(iatjl2), data_pos)
      call unimem_malloc(ptr_iatlj3, sizeof(iatlj3), data_pos)
      call unimem_malloc(ptr_iatlj1, sizeof(iatlj1), data_pos)
c-----------------------------MASS-------------------------------------
      call unimem_malloc(ptr_bm1,sizeof(bm1), data_pos)
      call unimem_malloc(ptr_bm2,sizeof(bm2), data_pos)
      call unimem_malloc(ptr_binvm1,sizeof(binvm1), data_pos)
      call unimem_malloc(ptr_bintm1,sizeof(bintm1), data_pos)
      call unimem_malloc(ptr_bm2inv,sizeof(bm2inv), data_pos)
      call unimem_malloc(ptr_baxm1,sizeof(baxm1), data_pos)
      call unimem_malloc(ptr_bm1lag,sizeof(bm1lag), data_pos)
      call unimem_malloc(ptr_volvm1,sizeof(volvm1), data_pos)
      call unimem_malloc(ptr_volvm2,sizeof(volvm2), data_pos)
      call unimem_malloc(ptr_voltm1,sizeof(voltm1), data_pos)
      call unimem_malloc(ptr_voltm2,sizeof(voltm2), data_pos)
      call unimem_malloc(ptr_yinvm1,sizeof(yinvm1), data_pos)
      call unimem_malloc(ptr_binvdg,sizeof(binvdg), data_pos)      

c----------------------------MVGEOM------------------------------
      call unimem_malloc(ptr_wx,sizeof(wx), data_pos)
      call unimem_malloc(ptr_wy,sizeof(wy), data_pos)
      call unimem_malloc(ptr_wz,sizeof(wz), data_pos)
      call unimem_malloc(ptr_wxlag,sizeof(wxlag), data_pos)
      call unimem_malloc(ptr_wylag,sizeof(wylag), data_pos)
      call unimem_malloc(ptr_wzlag,sizeof(wzlag), data_pos)
      call unimem_malloc(ptr_w1mask,sizeof(w1mask), data_pos)
      call unimem_malloc(ptr_w2mask,sizeof(w2mask), data_pos)
      call unimem_malloc(ptr_w3mask,sizeof(w3mask), data_pos)
      call unimem_malloc(ptr_wmult,sizeof(wmult), data_pos)
      call unimem_malloc(ptr_ev1,sizeof(ev1), data_pos)
      call unimem_malloc(ptr_ev2,sizeof(ev2), data_pos)
      call unimem_malloc(ptr_ev3,sizeof(ev3), data_pos)

c----------------------------PARALLEL-----------------------------
c---------ERROR!!!!!!!!
c      call unimem_malloc(ptr_nelgt_max, sizeof(nelgt_max), data_pos)
c      call unimem_malloc(ptr_nvtot, sizeof(nvtot), data_pos)
c      call unimem_malloc(ptr_nelg, sizeof(nelg), data_pos)
c      call unimem_malloc(ptr_lglel, sizeof(lglel), data_pos)
c      call unimem_malloc(ptr_gllel, sizeof(gllel), data_pos)
c      call unimem_malloc(ptr_gllnid, sizeof(gllnid), data_pos)
c      call unimem_malloc(ptr_nelgv, sizeof(nelgv), data_pos)
cc      call unimem_malloc(ptr_nelgt, sizeof(nelgt), data_pos)
c      call unimem_malloc(ptr_ifgprnt, sizeof(ifgprnt), data_pos)
c      call unimem_malloc(ptr_wdsize, sizeof(wdsize), data_pos)
c      call unimem_malloc(ptr_isize, sizeof(isize), data_pos)
c      call unimem_malloc(ptr_lsize, sizeof(lsize), data_pos)
c      call unimem_malloc(ptr_csize, sizeof(csize), data_pos)
c      call unimem_malloc(ptr_wdsizi, sizeof(wdsizi), data_pos)
c      call unimem_malloc(ptr_ifdblas, sizeof(ifdblas), data_pos)
c      call unimem_malloc(ptr_cr_h, sizeof(cr_h), data_pos)
c      call unimem_malloc(ptr_gsh, sizeof(gsh), data_pos)
c      call unimem_malloc(ptr_gsh_fld, sizeof(gsh_fld), data_pos)
c      call unimem_malloc(ptr_xxth, sizeof(xxth), data_pos)
c      call unimem_malloc(ptr_ifgsh_fld_same, sizeof(ifgsh_fld_same), 
c     $                   data_pos)
c      call unimem_malloc(ptr_dg_face, sizeof(dg_face), data_pos)
c      call unimem_malloc(ptr_dg_hndlx, sizeof(dg_hndlx), data_pos)
c      call unimem_malloc(ptr_ndg_facex, sizeof(ndg_facex), data_pos)

c-----------------------------SOLN-------------------------------------
c---------------------ERROR-------------------------------------------

      call unimem_malloc(ptr_bq, sizeof(bq), data_pos)

c      call unimem_malloc(ptr_vxlag,sizeof(vxlag), data_pos)
c      call unimem_malloc(ptr_vylag,sizeof(vylag), data_pos)
c      call unimem_malloc(ptr_vzlag,sizeof(vzlag), data_pos)
      call unimem_malloc(ptr_tlag,sizeof(tlag), data_pos)
      call unimem_malloc(ptr_vgradt1,sizeof(vgradt1), data_pos)
      call unimem_malloc(ptr_vgradt2,sizeof(vgradt2), data_pos)
      call unimem_malloc(ptr_abx1,sizeof(abx1), data_pos)
      call unimem_malloc(ptr_aby1,sizeof(aby1), data_pos)
      call unimem_malloc(ptr_abz1,sizeof(abz1), data_pos)
      call unimem_malloc(ptr_abx2,sizeof(abx2), data_pos)
      call unimem_malloc(ptr_aby2,sizeof(aby2), data_pos)
      call unimem_malloc(ptr_abz2,sizeof(abz2), data_pos)
      call unimem_malloc(ptr_vdiff_e,sizeof(vdiff_e), data_pos)


      call unimem_malloc(ptr_vx_copy, sizeof(vx_copy), -1)
      call unimem_malloc(ptr_vy_copy, sizeof(vy_copy), -1)
      call unimem_malloc(ptr_vz_copy, sizeof(vz_copy), -1)
      call unimem_malloc(ptr_vx_copy2, sizeof(vx_copy2), -1)
      call unimem_malloc(ptr_vy_copy2, sizeof(vy_copy2), -1)
      call unimem_malloc(ptr_vz_copy2, sizeof(vz_copy2), -1)


      call unimem_malloc(ptr_vx,sizeof(vx), 1)
      print *, "sizeof(vx)", sizeof(vx)

      call unimem_malloc(ptr_vy,sizeof(vy), 1)
            print *, "sizeof(vy)", sizeof(vy)

      call unimem_malloc(ptr_vz,sizeof(vz), 1)
      print *, "sizeof(vz)", sizeof(vz)
      
      call unimem_malloc(ptr_vx_e,sizeof(vx_e), data_pos)
      call unimem_malloc(ptr_vy_e,sizeof(vy_e), data_pos)
      call unimem_malloc(ptr_vz_e,sizeof(vz_e), data_pos)
      call unimem_malloc(ptr_t,sizeof(t), data_pos)
      call unimem_malloc(ptr_vtrans,sizeof(vtrans), data_pos)
      call unimem_malloc(ptr_vdiff,sizeof(vdiff), data_pos)
      call unimem_malloc(ptr_bfx,sizeof(bfx), data_pos)
      call unimem_malloc(ptr_bfy,sizeof(bfy), data_pos)
      call unimem_malloc(ptr_bfz,sizeof(bfz), data_pos)
      call unimem_malloc(ptr_cflf,sizeof(cflf), data_pos)
      call unimem_malloc(ptr_bmnv,sizeof(bmnv), data_pos)
      call unimem_malloc(ptr_bmass,sizeof(bmass), data_pos)
      call unimem_malloc(ptr_bdivw,sizeof(bdivw), data_pos)
      call unimem_malloc(ptr_c_vx,sizeof(c_vx), data_pos)
      call unimem_malloc(ptr_fw,sizeof(fw), data_pos)

      call unimem_malloc(ptr_bx,sizeof(bx), data_pos)
      call unimem_malloc(ptr_by,sizeof(by), data_pos)
      call unimem_malloc(ptr_bz,sizeof(bz), data_pos)
      call unimem_malloc(ptr_pm,sizeof(pm), data_pos)
      call unimem_malloc(ptr_bmx,sizeof(bmx), data_pos)
      call unimem_malloc(ptr_bmy,sizeof(bmy), data_pos)
      call unimem_malloc(ptr_bmz,sizeof(bmz), data_pos)
      call unimem_malloc(ptr_bbx1,sizeof(bbx1), data_pos)
      call unimem_malloc(ptr_bby1,sizeof(bby1), data_pos)
      call unimem_malloc(ptr_bbz1,sizeof(bbz1), data_pos)
      call unimem_malloc(ptr_bbx2,sizeof(bbx2), data_pos)
      call unimem_malloc(ptr_bby2,sizeof(bby2), data_pos)
      call unimem_malloc(ptr_bbz2,sizeof(bbz2), data_pos)
      call unimem_malloc(ptr_bxlag,sizeof(bxlag), data_pos)
      call unimem_malloc(ptr_bylag,sizeof(bylag), data_pos)
      call unimem_malloc(ptr_bzlag,sizeof(bzlag), data_pos)
      call unimem_malloc(ptr_pmlag,sizeof(pmlag), data_pos)

      call unimem_malloc(ptr_nu_star,sizeof(nu_star), data_pos)

      call unimem_malloc(ptr_pr_copy, sizeof(pr_copy), -1)
      call unimem_malloc(ptr_pr_copy2, sizeof(pr_copy2), -1)

      call unimem_malloc(ptr_pr,sizeof(pr), 1)
      print *,"sizeof(pr):", sizeof(pr)
      call unimem_malloc(ptr_pr_new,sizeof(pr),1)
      call unimem_malloc(ptr_pr_old,sizeof(pr),1)
      call unimem_malloc(ptr_prlag,sizeof(prlag), data_pos)
      call unimem_malloc(ptr_qt1,sizeof(qt1), data_pos)
      call unimem_malloc(ptr_usrdiv,sizeof(usrdiv), data_pos)
      call unimem_malloc(ptr_p0th,sizeof(p0th), data_pos)
      call unimem_malloc(ptr_dp0thdt,sizeof(dp0thdt), data_pos)
      call unimem_malloc(ptr_gamma0,sizeof(gamma0), data_pos)
      call unimem_malloc(ptr_v1mask,sizeof(v1mask), data_pos)
      call unimem_malloc(ptr_v2mask,sizeof(v2mask), data_pos)
      call unimem_malloc(ptr_v3mask,sizeof(v3mask), data_pos)
      call unimem_malloc(ptr_pmask,sizeof(pmask), data_pos)
      call unimem_malloc(ptr_tmask,sizeof(tmask), data_pos)
      call unimem_malloc(ptr_omask,sizeof(omask), data_pos)
      call unimem_malloc(ptr_vmult,sizeof(vmult), data_pos)
      call unimem_malloc(ptr_tmult,sizeof(tmult), data_pos)
      call unimem_malloc(ptr_b1mask,sizeof(b1mask), data_pos)
      call unimem_malloc(ptr_b2mask,sizeof(b2mask), data_pos)
      call unimem_malloc(ptr_b3mask,sizeof(b3mask), data_pos)
      call unimem_malloc(ptr_bpmask,sizeof(bpmask), data_pos)
      call unimem_malloc(ptr_vxp,sizeof(vxp), data_pos)
      call unimem_malloc(ptr_vyp,sizeof(vyp), data_pos)
      call unimem_malloc(ptr_vzp,sizeof(vzp), data_pos)
      call unimem_malloc(ptr_prp,sizeof(prp), data_pos)
      call unimem_malloc(ptr_tp,sizeof(tp), data_pos)
      call unimem_malloc(ptr_bqp,sizeof(bqp), data_pos)
      call unimem_malloc(ptr_bfxp,sizeof(bfxp), data_pos)
      call unimem_malloc(ptr_bfyp,sizeof(bfyp), data_pos)
      call unimem_malloc(ptr_bfzp,sizeof(bfzp), data_pos)
      call unimem_malloc(ptr_vxlagp,sizeof(vxlagp), data_pos)
      call unimem_malloc(ptr_vylagp,sizeof(vylagp), data_pos)
      call unimem_malloc(ptr_vzlagp,sizeof(vzlagp), data_pos)
      call unimem_malloc(ptr_prlagp,sizeof(prlagp), data_pos)
      call unimem_malloc(ptr_tlagp,sizeof(tlagp), data_pos)
      call unimem_malloc(ptr_exx1p,sizeof(exx1p), data_pos)
      call unimem_malloc(ptr_exy1p,sizeof(exy1p), data_pos)
      call unimem_malloc(ptr_exz1p,sizeof(exz1p), data_pos)
      call unimem_malloc(ptr_exx2p,sizeof(exx2p), data_pos)
      call unimem_malloc(ptr_exy2p,sizeof(exy2p), data_pos)
      call unimem_malloc(ptr_exz2p,sizeof(exz2p), data_pos)
      call unimem_malloc(ptr_vgradt1p,sizeof(vgradt1p), data_pos)
      call unimem_malloc(ptr_vgradt2p,sizeof(vgradt2p), data_pos)
      call unimem_malloc(ptr_jp,sizeof(jp), data_pos)


c----------------------STEADY------------------------------------------
      call unimem_malloc(ptr_tauss, sizeof(tauss), data_pos)
      call unimem_malloc(ptr_txnext, sizeof(txnext), data_pos)
      call unimem_malloc(ptr_nsskip, sizeof(nsskip), data_pos)
      call unimem_malloc(ptr_ifskip, sizeof(ifskip), data_pos)
      call unimem_malloc(ptr_ifmodp, sizeof(ifmodp), data_pos)
      call unimem_malloc(ptr_ifssvt, sizeof(ifssvt), data_pos)
      call unimem_malloc(ptr_ifstst, sizeof(ifstst), data_pos)
      call unimem_malloc(ptr_ifexvt, sizeof(ifexvt), data_pos)
      call unimem_malloc(ptr_ifextr, sizeof(ifextr), data_pos)
      call unimem_malloc(ptr_dvnnh1, sizeof(dvnnh1), data_pos)
      call unimem_malloc(ptr_dvnnsm, sizeof(dvnnsm), data_pos)
      call unimem_malloc(ptr_dvnn12, sizeof(dvnn12), data_pos)
      call unimem_malloc(ptr_dvnn18, sizeof(dvnn18), data_pos)
      call unimem_malloc(ptr_dvdfh1, sizeof(dvdfh1), data_pos)
      call unimem_malloc(ptr_dvdfsm, sizeof(dvdfsm), data_pos)
      call unimem_malloc(ptr_dvdf12, sizeof(dvdf12), data_pos)
      call unimem_malloc(ptr_dvdf18, sizeof(dvdf18), data_pos)
      call unimem_malloc(ptr_dvprh1, sizeof(dvprh1), data_pos)
      call unimem_malloc(ptr_dvprsm, sizeof(dvprsm), data_pos)
      call unimem_malloc(ptr_dvpr12, sizeof(dvpr12), data_pos)
      call unimem_malloc(ptr_dvpr18, sizeof(dvpr18), data_pos)

c----------------------TOPOL-------------------------------------------
      call unimem_malloc(ptr_nomlis, sizeof(nomlis), data_pos)
      call unimem_malloc(ptr_nmlinv, sizeof(nmlinv), data_pos)
      call unimem_malloc(ptr_group, sizeof(group), data_pos)
      call unimem_malloc(ptr_skpdat, sizeof(skpdat), data_pos)
      call unimem_malloc(ptr_eface, sizeof(eface), data_pos)
      call unimem_malloc(ptr_eface1, sizeof(eface1), data_pos)
      call unimem_malloc(ptr_eskip, sizeof(eskip), data_pos)
      call unimem_malloc(ptr_nedg, sizeof(nedg), data_pos)
      call unimem_malloc(ptr_ncmp, sizeof(ncmp), data_pos)
      call unimem_malloc(ptr_ixcn, sizeof(ixcn), data_pos)
      call unimem_malloc(ptr_noffst, sizeof(noffst), data_pos)
      call unimem_malloc(ptr_maxmlt, sizeof(maxmlt), data_pos)
      call unimem_malloc(ptr_nspmax, sizeof(nspmax), data_pos)
      call unimem_malloc(ptr_ngspcn, sizeof(ngspcn), data_pos)
      call unimem_malloc(ptr_ngsped, sizeof(ngsped), data_pos)
      call unimem_malloc(ptr_numscn , sizeof(numscn), data_pos)
      call unimem_malloc(ptr_numsed, sizeof(numsed), data_pos)
      call unimem_malloc(ptr_gcnnum, sizeof(gcnnum), data_pos)
      call unimem_malloc(ptr_lcnnum, sizeof(lcnnum), data_pos)
      call unimem_malloc(ptr_gednum, sizeof(gednum), data_pos)
      call unimem_malloc(ptr_lednum, sizeof(lednum), data_pos)
      call unimem_malloc(ptr_gedtyp, sizeof(gedtyp), data_pos)
      call unimem_malloc(ptr_ngcomm, sizeof(ngcomm), data_pos)
c      call unimem_malloc(ptr_iedge, sizeof(iedge), data_pos)
c      call unimem_malloc(ptr_iedgef, sizeof(iedgef), data_pos)
cc      call unimem_malloc(ptr_icedg, sizeof(icedg), data_pos)
cc      call unimem_malloc(ptr_iedgfc, sizeof(iedgfc), data_pos)
cc      call unimem_malloc(ptr_icface, sizeof(icface), data_pos)
c      call unimem_malloc(ptr_indx, sizeof(indx), data_pos)
c      call unimem_malloc(ptr_invedg, sizeof(invedg), data_pos)

c---------------------------TSTEP--------------------------------------
c      call unimem_malloc(ptr_time,sizeof(time), data_pos)
c      call unimem_malloc(ptr_timef,sizeof(timef), data_pos)
c      call unimem_malloc(ptr_fintim,sizeof(fintim), data_pos)
c      call unimem_malloc(ptr_timeio,sizeof(timeio), data_pos)
c      call unimem_malloc(ptr_dt,sizeof(dt), data_pos)
c      call unimem_malloc(ptr_dtlag,sizeof(dtlag), data_pos)
c      call unimem_malloc(ptr_dtinit,sizeof(dtinit), data_pos)
c      call unimem_malloc(ptr_dtinvm,sizeof(dtinvm), data_pos)
c      call unimem_malloc(ptr_courno,sizeof(courno), data_pos)
c      call unimem_malloc(ptr_ctarg,sizeof(ctarg), data_pos)
c      call unimem_malloc(ptr_ab,sizeof(ab), data_pos)
c      call unimem_malloc(ptr_bd,sizeof(bd), data_pos)
c      call unimem_malloc(ptr_abmsh,sizeof(abmsh), data_pos)
c      call unimem_malloc(ptr_avdiff,sizeof(avdiff), data_pos)
c      call unimem_malloc(ptr_avtran,sizeof(avtran), data_pos)
c      call unimem_malloc(ptr_volfld,sizeof(volfld), data_pos)
c      call unimem_malloc(ptr_tolrel,sizeof(tolrel), data_pos)
c      call unimem_malloc(ptr_tolabs,sizeof(tolabs), data_pos)
c      call unimem_malloc(ptr_tolhdf,sizeof(tolhdf), data_pos)
c      call unimem_malloc(ptr_tolpdf,sizeof(tolpdf), data_pos)
c      call unimem_malloc(ptr_tolev,sizeof(tolev), data_pos)
c      call unimem_malloc(ptr_tolnl,sizeof(tolnl), data_pos)
c      call unimem_malloc(ptr_prelax,sizeof(prelax), data_pos)
c      call unimem_malloc(ptr_tolps,sizeof(tolps), data_pos)
c      call unimem_malloc(pt_tolhs,sizeof(tolhs), data_pos)
c      call unimem_malloc(ptr_tolhr,sizeof(tolhr), data_pos)
c      call unimem_malloc(ptr_tolhv,sizeof(tolhv), data_pos)
c      call unimem_malloc(ptr_tolht,sizeof(tolht), data_pos)
c      call unimem_malloc(ptr_tolhe,sizeof(tolhe), data_pos)
c      call unimem_malloc(ptr_vnrmh1,sizeof(vnrmh1), data_pos)
c      call unimem_malloc(ptr_vnrmsm,sizeof(vnrmsm), data_pos)
c      call unimem_malloc(ptr_vnrm12,sizeof(vnrm12), data_pos)
c      call unimem_malloc(ptr_vnrm18,sizeof(vnrm18), data_pos)
c      call unimem_malloc(ptr_vmean,sizeof(vmean), data_pos)
c      call unimem_malloc(ptr_tnrmh1,sizeof(tnrmh1), data_pos)
c      call unimem_malloc(ptr_tnrmsm,sizeof(tnrmsm), data_pos)
c      call unimem_malloc(ptr_tnrm12,sizeof(tnrm12), data_pos)
c      call unimem_malloc(ptr_tnrm18,sizeof(tnrm18), data_pos)
c      call unimem_malloc(ptr_tmean,sizeof(tmean), data_pos)

c--------------------TURBO--------------------------------------------
      call unimem_malloc(ptr_vturb,sizeof(vturb), data_pos)
      call unimem_malloc(ptr_turbl,sizeof(turbl), data_pos)
      call unimem_malloc(ptr_uwall,sizeof(uwall), data_pos)
      call unimem_malloc(ptr_zwall,sizeof(zwall), data_pos)
      call unimem_malloc(ptr_twx,sizeof(twx), data_pos)
      call unimem_malloc(ptr_twy,sizeof(twy), data_pos)
      call unimem_malloc(ptr_twz,sizeof(twz), data_pos)

      call unimem_malloc(ptr_cmu, sizeof(cmu), data_pos)
      call unimem_malloc(ptr_cmt, sizeof(cmt), data_pos)
      call unimem_malloc(ptr_sgk, sizeof(sgk), data_pos)
      call unimem_malloc(ptr_ce1, sizeof(ce1), data_pos)
      call unimem_malloc(ptr_ce2, sizeof(ce2), data_pos)
      call unimem_malloc(ptr_vkc, sizeof(vkc), data_pos)
      call unimem_malloc(ptr_bta, sizeof(bta), data_pos)
      call unimem_malloc(ptr_sgt, sizeof(sgt), data_pos)
      call unimem_malloc(ptr_beta1, sizeof(beta1), data_pos)
      call unimem_malloc(ptr_beta2, sizeof(beta2), data_pos)
      call unimem_malloc(ptr_cmi, sizeof(cmi), data_pos)
      call unimem_malloc(ptr_ski, sizeof(ski), data_pos)
      call unimem_malloc(ptr_sei, sizeof(sei), data_pos)
      call unimem_malloc(ptr_vki, sizeof(vki), data_pos)
      call unimem_malloc(ptr_bti, sizeof(bti), data_pos)
      call unimem_malloc(ptr_sti, sizeof(sti), data_pos)
      call unimem_malloc(ptr_zpldat, sizeof(zpldat), data_pos)
      call unimem_malloc(ptr_zpudat, sizeof(zpudat), data_pos)
      call unimem_malloc(ptr_zpvdat, sizeof(zpvdat), data_pos)
      call unimem_malloc(ptr_tlmax, sizeof(tlmax), data_pos)
      call unimem_malloc(ptr_tlimul, sizeof(tlimul), data_pos)

      call unimem_malloc(ptr_ifldk, sizeof(ifldk), data_pos)
      call unimem_malloc(ptr_ifldtk, sizeof(ifltdk), data_pos)
      call unimem_malloc(ptr_iflde, sizeof(iflde), data_pos)
      call unimem_malloc(ptr_ifldte, sizeof(ifldte), data_pos)
      call unimem_malloc(ptr_ifswall, sizeof(ifswall), data_pos)
      call unimem_malloc(ptr_iftwsh, sizeof(iftwsh), data_pos)
      call unimem_malloc(ptr_ifcwuz, sizeof(ifcwuz), data_pos)

c--------------------------ESOLV----------------------------------------
      call unimem_malloc(ptr_iesolv, sizeof(iesolv), data_pos)
      call unimem_malloc(ptr_ifalgn, sizeof(ifalgn), data_pos)
      call unimem_malloc(ptr_ifrsxy, sizeof(ifrsxy), data_pos)
      call unimem_malloc(ptr_volel, sizeof(volel), data_pos)
C--------------------------WZ------------------------------------------
      call unimem_malloc(ptr_zgm1, sizeof(zgm1), data_pos)
      call unimem_malloc(ptr_zgm2, sizeof(zgm2), data_pos)
      call unimem_malloc(ptr_zgm3, sizeof(zgm3), data_pos)
      call unimem_malloc(ptr_zam1, sizeof(zam1), data_pos)
      call unimem_malloc(ptr_zam2, sizeof(zam2), data_pos)
      call unimem_malloc(ptr_zam3, sizeof(zam3), data_pos)
      call unimem_malloc(ptr_wxm1, sizeof(wxm1), data_pos)
      call unimem_malloc(ptr_wym1, sizeof(wym1), data_pos)
      call unimem_malloc(ptr_wzm1, sizeof(wzm1), data_pos)
      call unimem_malloc(ptr_w3m1, sizeof(w3m1), data_pos)
      call unimem_malloc(ptr_wxm2, sizeof(wxm2), data_pos)
      call unimem_malloc(ptr_wym2, sizeof(wym2), data_pos)
      call unimem_malloc(ptr_wzm2, sizeof(wzm2), data_pos)
      call unimem_malloc(ptr_w3m2, sizeof(w3m2), data_pos)
      call unimem_malloc(ptr_wxm3, sizeof(wxm3), data_pos)
      call unimem_malloc(ptr_wym3, sizeof(wym3), data_pos)
      call unimem_malloc(ptr_wzm3, sizeof(wzm3), data_pos)
      call unimem_malloc(ptr_w3m3, sizeof(w3m3), data_pos)
      call unimem_malloc(ptr_wam1, sizeof(wam1), data_pos)
      call unimem_malloc(ptr_wam2, sizeof(wam2), data_pos)
      call unimem_malloc(ptr_wam3, sizeof(wam3), data_pos)
      call unimem_malloc(ptr_w2am1, sizeof(w2am1), data_pos)
      call unimem_malloc(ptr_w2cm1, sizeof(w2cm1), data_pos)
      call unimem_malloc(ptr_w2am2, sizeof(w2am2), data_pos)
      call unimem_malloc(ptr_w2cm2, sizeof(w2cm2), data_pos)
      call unimem_malloc(ptr_w2am3, sizeof(w2am3), data_pos)
      call unimem_malloc(ptr_w2cm3, sizeof(w2cm3), data_pos)

c-------------------------WZF------------------------------------------
      call unimem_malloc(ptr_lxq, sizeof(lxq), data_pos)
      call unimem_malloc(ptr_zgl, sizeof(zgl), data_pos)
      call unimem_malloc(ptr_wgl, sizeof(wgl), data_pos)
      call unimem_malloc(ptr_zgp, sizeof(zgp), data_pos)
      call unimem_malloc(ptr_wgp, sizeof(wgp), data_pos)
      call unimem_malloc(ptr_wgl1, sizeof(wgl1), data_pos)
      call unimem_malloc(ptr_wgl2, sizeof(wgl2), data_pos)
      call unimem_malloc(ptr_wgli, sizeof(wgli), data_pos)
      call unimem_malloc(ptr_d1, sizeof(d1), data_pos)
      call unimem_malloc(ptr_d1t, sizeof(d1t), data_pos)
      call unimem_malloc(ptr_d2, sizeof(d2), data_pos)
      call unimem_malloc(ptr_b2p, sizeof(b2p), data_pos)
      call unimem_malloc(ptr_B1iA1, sizeof(B1iA1), data_pos)
      call unimem_malloc(ptr_B1iA1t, sizeof(B1iA1t), data_pos)
      call unimem_malloc(ptr_da, sizeof(da), data_pos)
      call unimem_malloc(ptr_dat, sizeof(dat), data_pos)
      call unimem_malloc(ptr_iggl, sizeof(iggl), data_pos)
      call unimem_malloc(ptr_igglt, sizeof(igglt), data_pos)
      call unimem_malloc(ptr_dglg, sizeof(dglg), data_pos)
      call unimem_malloc(ptr_dglgt, sizeof(dglgt), data_pos)
      call unimem_malloc(ptr_wglgt, sizeof(wglgt), data_pos)
      call unimem_malloc(ptr_wglg, sizeof(wglg), data_pos)
c-----------------DOMAIN----------------------------------------------
      call unimem_malloc(ptr_ndom, sizeof(ndom), data_pos)
      call unimem_malloc(ptr_n_o, sizeof(n_o), data_pos)
      call unimem_malloc(ptr_nel_proc, sizeof(nel_proc), data_pos)
      call unimem_malloc(ptr_gs_hnd_overlap, sizeof(gs_hnd_overlap), 
     $                   data_pos)
      call unimem_malloc(ptr_na, sizeof(na), data_pos)
      call unimem_malloc(ptr_ma, sizeof(ma), data_pos)
      call unimem_malloc(ptr_nza, sizeof(nza), data_pos)
      call unimem_malloc(ptr_ltotd, sizeof(ltotd), data_pos)
      call unimem_malloc(ptr_lxc, sizeof(lxc), data_pos)
      call unimem_malloc(ptr_lcr, sizeof(lcr), data_pos)
      call unimem_malloc(ptr_se_to_gcrs, sizeof(se_to_gcrs), data_pos)
      call unimem_malloc(ptr_n_crs, sizeof(n_crs), data_pos)
      call unimem_malloc(ptr_m_crs, sizeof(m_crs), data_pos)
      call unimem_malloc(ptr_nx_crs, sizeof(nx_crs), data_pos)
c      call unimem_malloc(ptr_nxyz_c, sizeof(nxyz_c), data_pos)
c      call unimem_malloc(ptr_h1_basis, sizeof(h1_basis), data_pos)
c      call unimem_malloc(ptr_h1_basist, sizeof(h1_basist), data_pos)

c--------------------ZPER----------------------------------------------
      call unimem_malloc(ptr_lelg_sm, sizeof(lelg_sm), data_pos)
      call unimem_malloc(ptr_ltfdm2, sizeof(ltfdm2), data_pos)
      call unimem_malloc(ptr_leig2, sizeof(leig2), data_pos)
      call unimem_malloc(ptr_leig, sizeof(leig), data_pos)
      call unimem_malloc(ptr_lfdm0, sizeof(lfdm0), data_pos)

      call unimem_malloc(ptr_neigx, sizeof(neigx), data_pos)
      call unimem_malloc(ptr_neigy, sizeof(neigy), data_pos)
      call unimem_malloc(ptr_neigz, sizeof(neigz), data_pos)
      call unimem_malloc(ptr_pvalx, sizeof(pvalx), data_pos)
      call unimem_malloc(ptr_pvaly, sizeof(pvaly), data_pos)
      call unimem_malloc(ptr_pvalz, sizeof(pvalz), data_pos)
      call unimem_malloc(ptr_pvecx, sizeof(pvecx), data_pos)
      call unimem_malloc(ptr_pvecy, sizeof(pvecy), data_pos)
      call unimem_malloc(ptr_pvecz, sizeof(pvecz), data_pos)
      call unimem_malloc(ptr_sp, sizeof(sp), data_pos)
      call unimem_malloc(ptr_spt, sizeof(spt), data_pos)
      call unimem_malloc(ptr_eigp, sizeof(eigp), data_pos)
      call unimem_malloc(ptr_wavep, sizeof(wavep), data_pos)
      call unimem_malloc(ptr_msp, sizeof(msp), data_pos)
      call unimem_malloc(ptr_mlp, sizeof(mlp), data_pos)
      call unimem_malloc(ptr_ifycrv, sizeof(ifycrv), data_pos)
      call unimem_malloc(ptr_ifzper, sizeof(ifzper), data_pos)
      call unimem_malloc(ptr_ifgfdm, sizeof(ifgfdm), data_pos)
      call unimem_malloc(ptr_ifgtp, sizeof(ifgto), data_pos)
      call unimem_malloc(ptr_ifemat, sizeof(ifemat), data_pos)

c      call unimem_malloc(ptr_nelx, sizeof(nelx), data_pos)
c      call unimem_malloc(ptr_nely, sizeof(nely), data_pos)
c      call unimem_malloc(ptr_nelz, sizeof(nelz), data_pos)
c      call unimem_malloc(ptr_nelxy, sizeof(nelxy), data_pos)
      call unimem_malloc(ptr_lex2pst, sizeof(lex2pst), data_pos)
      call unimem_malloc(ptr_pst2lex, sizeof(pst2lex), data_pos)
      call unimem_malloc(ptr_ngfdm_p, sizeof(ngfdm_P), data_pos)
      call unimem_malloc(ptr_ngfdm_v, sizeof(ngfdm_v), data_pos)
      call unimem_malloc(ptr_lp_small, sizeof(lp_small), data_pos)
      call unimem_malloc(ptr_part_in, sizeof(part_in), data_pos)
      call unimem_malloc(ptr_part_out, sizeof(part_out), data_pos)
      call unimem_malloc(ptr_msg_id, sizeof(msg_id), data_pos)
      call unimem_malloc(ptr_mcex, sizeof(mcex), data_pos)
      call unimem_malloc(ptr_tpn1, sizeof(tpn1), data_pos)
      call unimem_malloc(ptr_tpn2, sizeof(tpn2), data_pos)
      call unimem_malloc(ptr_tpn3, sizeof(tpn3), data_pos)
      call unimem_malloc(ptr_ind23, sizeof(ind23), data_pos)
      call unimem_malloc(ptr_lfdx, sizeof(lfdx), data_pos)
      call unimem_malloc(ptr_lfdy, sizeof(lfdy), data_pos)
      call unimem_malloc(ptr_lfdz, sizeof(lfdz), data_pos)
      call unimem_malloc(ptr_xgtp, sizeof(xgtp), data_pos)
      call unimem_malloc(ptr_ygtp, sizeof(ygtp), data_pos)
      call unimem_malloc(ptr_zgtp, sizeof(zgtp), data_pos)
      call unimem_malloc(ptr_xmlt, sizeof(xmlt), data_pos)
      call unimem_malloc(ptr_ymlt, sizeof(ymlt), data_pos)
      call unimem_malloc(ptr_zmlt, sizeof(zmlt), data_pos)

      call unimem_malloc(ptr_rx2, sizeof(rx2), data_pos)
      call unimem_malloc(ptr_ry2, sizeof(ry2), data_pos)
      call unimem_malloc(ptr_sx2, sizeof(sx2), data_pos)
      call unimem_malloc(ptr_sy2, sizeof(sy2), data_pos)
      call unimem_malloc(ptr_w2d, sizeof(w2d), data_pos)
      call unimem_malloc(ptr_bxyi, sizeof(bxyi), data_pos)
      call unimem_malloc(ptr_gtp_cbc, sizeof(gtp_cbc), data_pos)

c-----------------------OPCTR------------------------------------------
      call unimem_malloc(ptr_maxrts, sizeof(maxrts), data_pos)
      call unimem_malloc(ptr_rname, sizeof(rname), data_pos)
      call unimem_malloc(ptr_dct, sizeof(dct), data_pos)
      call unimem_malloc(ptr_rct, sizeof(rct), data_pos)
      call unimem_malloc(ptr_dcount, sizeof(dcount), data_pos)
      call unimem_malloc(ptr_ncall, sizeof(ncall), data_pos)
      call unimem_malloc(ptr_nrout, sizeof(nrout), data_pos)
C---------------------CTIEMR-------------------------------------------
      
c-------------------------openmp-----------------------------------
c       call c_openmp_init(4)


      return 
      end
c-----------------------------------------------------------------------
      subroutine nek_init(intracomm)
c

      include 'SIZE'
      include 'TOTAL'
      include 'DOMAIN'
      include 'ZPER'
c
      include 'OPCTR'
      include 'CTIMER'

C     used scratch arrays
C     NOTE: no initial declaration needed. Linker will take 
c           care about the size of the CBs automatically
c
c      COMMON /CTMP1/ DUMMY1(LCTMP1)
c      COMMON /CTMP0/ DUMMY0(LCTMP0)
c
c      COMMON /SCRNS/ DUMMY2(LX1,LY1,LZ1,LELT,7)
c      COMMON /SCRUZ/ DUMMY3(LX1,LY1,LZ1,LELT,4)
c      COMMON /SCREV/ DUMMY4(LX1,LY1,LZ1,LELT,2)
c      COMMON /SCRVH/ DUMMY5(LX1,LY1,LZ1,LELT,2)
c      COMMON /SCRMG/ DUMMY6(LX1,LY1,LZ1,LELT,4)
c      COMMON /SCRCH/ DUMMY7(LX1,LY1,LZ1,LELT,2)
c      COMMON /SCRSF/ DUMMY8(LX1,LY1,LZ1,LELT,3)
c      COMMON /SCRCG/ DUMM10(LX1,LY1,LZ1,LELT,1)
  
      common /rdump/ ntdump

      real kwave2
      real*8 t0, tpp

      logical ifemati,ifsync_

      call get_session_info(intracomm)

      etimes = dnekclock()
      istep  = 0
      tpp    = 0.0

      call opcount(1)

      call initdim         ! Initialize / set default values.
      call initdat
      call files

      etime = dnekclock()
      call readat          ! Read .rea +map file
      etims0 = dnekclock_sync()
      if (nio.eq.0) then
         write(6,12) 'nelgt/nelgv/lelt:',nelgt,nelgv,lelt
         write(6,12) 'lx1  /lx2  /lx3 :',lx1,lx2,lx3
         write(6,'(A,g13.5,A,/)')  ' done :: read .rea file ',
     &                             etims0-etime,' sec'
 12      format(1X,A,4I12,/,/)
      endif 

      ifsync_ = ifsync
      ifsync = .true.

      call setvar          ! Initialize most variables

      instep=1             ! Check for zero steps
      if (nsteps.eq.0 .and. fintim.eq.0.) instep=0

      igeom = 2
      call setup_topo      ! Setup domain topology  

      call genwz           ! Compute GLL points, weights, etc.

      call io_init         ! Initalize io unit

      if(nio.eq.0) write(6,*) 'call usrdat'
      call usrdat
      if(nio.eq.0) write(6,'(A,/)') ' done :: usrdat' 

      call gengeom(igeom)  ! Generate geometry, after usrdat 

      if (ifmvbd) call setup_mesh_dssum ! Set mesh dssum (needs geom)

      if(nio.eq.0) write(6,*) 'call usrdat2'
      call usrdat2
      if(nio.eq.0) write(6,'(A,/)') ' done :: usrdat2' 

      call geom_reset(1)    ! recompute Jacobians, etc.
      call vrdsmsh          ! verify mesh topology

      call setlog  ! Initalize logical flags

      call bcmask  ! Set BC masks for Dirichlet boundaries.

      if (fintim.ne.0.0.or.nsteps.ne.0) 
     $   call geneig(igeom) ! eigvals for tolerances

      call vrdsmsh     !     Verify mesh topology

      call dg_setup    !     Setup DG, if dg flag is set.

      if (ifflow.and.(fintim.ne.0.or.nsteps.ne.0)) then    ! Pressure solver 
         call estrat                                       ! initialization.
         if (iftran.and.solver_type.eq.'itr') then         ! Uses SOLN space 
            call set_overlap                               ! as scratch!
         elseif (solver_type.eq.'fdm'.or.solver_type.eq.'pdm')then
            ifemati = .true.
            kwave2  = 0.0
            if (ifsplit) ifemati = .false.
            call gfdm_init(nx2,ny2,nz2,ifemati,kwave2)
         elseif (solver_type.eq.'25D') then
            call g25d_init
         endif
      endif

      if(ifcvode) call cv_setsize

      if(nio.eq.0) write(6,*) 'call usrdat3'
      call usrdat3
      if(nio.eq.0) write(6,'(A,/)') ' done :: usrdat3'

#ifdef CMTNEK
        call nek_cmt_init
        if (nio.eq.0) write(6,*)'Initialized DG machinery'
#endif

      call setics      !     Set initial conditions 
      call setprop     !     Compute field properties

      if (instep.ne.0) then !USRCHK
        if(nio.eq.0) write(6,*) 'call userchk'
         if (ifneknek) call userchk_set_xfer
         if (ifneknek) call bcopy
         if (ifneknek) call chk_outflow
         call userchk
         if(nio.eq.0) write(6,'(A,/)') ' done :: userchk' 
      endif

      if (ifcvode .and. nsteps.gt.0) call cv_init

      call comment
      call sstest (isss) 

      call dofcnt

      jp = 0            ! Set perturbation field count to 0 for baseline flow

      call in_situ_init()

      call time00       !     Initalize timers to ZERO
      call opcount(2)

      ntdump=0
      if (timeio.ne.0.0) ntdump = int( time/timeio )

      etims0 = dnekclock_sync()
      if (nio.eq.0) then
        write (6,*) ' '
        if (time.ne.0.0) write (6,'(a,e14.7)') ' Initial time:',time
        write (6,'(a,g13.5,a)') 
     &              ' Initialization successfully completed ',
     &              etims0-etimes, ' sec'
      endif

      ifsync = ifsync_ ! restore initial value

      return
      end
c-----------------------------------------------------------------------
      subroutine nek_solve

      include 'SIZE'    
      include 'TSTEP'
      include 'INPUT'
      include 'CTIMER'
      include 'SOLN'
      include 'GEOM'

      real*4 papi_mflops
      integer*8 papi_flops
c------------kai------------------
      real tmp_unit

      tmp_unit = 1
c      print *, "nid = ", nid
c--------------------------------
      call nekgsync()

      if (instep.eq.0) then
        if(nid.eq.0) write(6,'(/,A,/,A,/)') 
     &     ' nsteps=0 -> skip time loop',
     &     ' running solver in post processing mode'
      else
        if(nio.eq.0) write(6,'(/,A,/)') 'Starting time loop ...'
      endif

      isyc  = 0
      if(ifsync) isyc=1
      itime = 0
#ifdef TIMER
      itime = 1
#endif
      call nek_comm_settings(isyc,itime)
      call nek_comm_startstat()

      istep  = 0
      msteps = 1
c--ychuang
        nsteps=30
      do kstep=1,nsteps,msteps

         call nek__multi_advance(ptr_pr_new,ptr_pr_old, kstep,msteps)
         call c_switch(ptr_pr_new,ptr_pr_old,sizeof(pr))
c-------------kai--------------------------------------------------                                                                               
c         print *, "sizeof(xm1)", sizeof(xm1)
        call c_dram_cache_cp(ptr_xm1_copy, ptr_xm1, sizeof(xm1))                                                                                 
        call c_dram_cache_cp(ptr_ym1_copy, ptr_ym1, sizeof(ym1))                                                                                 
        call c_dram_cache_cp(ptr_zm1_copy, ptr_zm1, sizeof(zm1))                                                                                 
        call c_dram_cache_cp(ptr_vx_copy, ptr_vx, sizeof(vx))                                                                                    
        call c_dram_cache_cp(ptr_vy_copy, ptr_vy, sizeof(vy))                                                                                    
        call c_dram_cache_cp(ptr_vz_copy, ptr_vz, sizeof(vz))                                                                                    
        call c_dram_cache_cp(ptr_pr_copy, ptr_pr, sizeof(pr))                                                                                    

c        call c_memcpy(ptr_xm1_copy2, ptr_xm1_copy, 
c     >        sizeof(xm1_copy))                                                                                  
c        call c_memcpy(ptr_ym1_copy2, ptr_ym1_copy, 
c     >       sizeof(ym1_copy))                                                                                 
c        call c_memcpy(ptr_zm1_copy2, ptr_zm1_copy, 
c     >       sizeof(zm1_copy))                                                                                  
c        call c_memcpy(ptr_vx_copy2, ptr_vx_copy, 
c     >       sizeof(vx_copy))                                                                                     
c        call c_memcpy(ptr_vy_copy2, ptr_vy_copy, 
c     >       sizeof(vy_copy))                                                                                     
c        call c_memcpy(ptr_vz_copy2, ptr_vz_copy, 
c     >       sizeof(vz_copy))                                                                                     
c        call c_memcpy(ptr_pr_copy2, ptr_pr_copy, 
c     >       sizeof(pr_copy)) 

c         call c_dram_cache_move(ptr_xm1_copy, ptr_xm1, sizeof(xm1))
c         call c_dram_cache_move(ptr_ym1_copy, ptr_ym1, sizeof(ym1))
c         call c_dram_cache_move(ptr_zm1_copy, ptr_zm1, sizeof(zm1))
c         call c_dram_cache_move(ptr_vx_copy, ptr_vx, sizeof(vx))
c         call c_dram_cache_move(ptr_vy_copy, ptr_vy, sizeof(vy))
c         call c_dram_cache_move(ptr_vz_copy, ptr_vz, sizeof(vz))
c         call c_dram_cache_move(ptr_pr_copy, ptr_pr, sizeof(pr))

c         call c_memmove_movnt(ptr_xm1_copy2, ptr_xm1_copy, 
c     >                        sizeof(xm1_copy))
c         call c_memmove_movnt(ptr_xm1_copy2, ptr_xm1_copy, 
c     >                        sizeof(ym1_copy))
c         call c_memmove_movnt(ptr_xm1_copy2,ptr_xm1_copy, 
c     >                        sizeof(zm1_copy))
c         call c_memmove_movnt(ptr_vx_copy2, ptr_vx_copy,
c     >                        sizeof(vx_copy))
c         call c_memmove_movnt(ptr_vy_copy2, ptr_vy_copy,
c     >                        sizeof(vy_copy))
c         call c_memmove_movnt(ptr_vz_copy2, ptr_vz_copy,
c     >                        sizeof(vz_copy))
c         call c_memmove_movnt(ptr_pr_copy2, ptr_pr_copy,
c     >                        sizeof(pr_copy))

c--------------memory_based_write
c         call c_memwrite(ptr_xm1_copy, sizeof(tmp_unit), 
c     >       sizeof(xm1_copy), nid, 1)                                                     
c         call c_memwrite(ptr_ym1_copy, sizeof(tmp_unit),
c     >       sizeof(ym1_copy), nid, 1)  
c         call c_memwrite(ptr_zm1_copy, sizeof(tmp_unit),
c     >       sizeof(zm1_copy), nid, 1)  
c         call c_memwrite(ptr_vx_copy, sizeof(tmp_unit),
c     >       sizeof(vx_copy), nid, 1)
c         call c_memwrite(ptr_vy_copy, sizeof(tmp_unit),
c     >       sizeof(vy_copy), nid, 1)
c         call c_memwrite(ptr_vz_copy, sizeof(tmp_unit),
c     >       sizeof(vz_copy), nid, 1)
c         call c_memwrite(ptr_pr_copy, sizeof(tmp_unit),
c     >       sizeof(pr_copy), nid, 1)

c---------------disk_write_local  
c         call c_memwrite(ptr_xm1_copy, sizeof(tmp_unit),
c     >       sizeof(xm1_copy), nid, 1)                                                                                                           
c         call c_memwrite(ptr_ym1_copy, sizeof(tmp_unit),
c     >       sizeof(ym1_copy), nid, 3)
c         call c_memwrite(ptr_zm1_copy, sizeof(tmp_unit),
c     >       sizeof(zm1_copy), nid, 3)
c         call c_memwrite(ptr_vx_copy, sizeof(tmp_unit),
c     >       sizeof(vx_copy), nid, 3)
c         call c_memwrite(ptr_vy_copy, sizeof(tmp_unit),
c     >       sizeof(vy_copy), nid, 3)
c         call c_memwrite(ptr_vz_copy, sizeof(tmp_unit),
c     >       sizeof(vz_copy), nid, 3)
c         call c_memwrite(ptr_pr_copy, sizeof(tmp_unit),
c     >       sizeof(pr_copy), nid, 3)
c         call c_memwrite(ptr_xm1_copy, sizeof(tmp_unit),
c     >       sizeof(xm1_copy), nid, 3)       
c-------------------disk_write_remote 
         call c_memwrite(ptr_ym1_copy, sizeof(tmp_unit),
     >       sizeof(ym1_copy), nid, 2)
         call c_memwrite(ptr_zm1_copy, sizeof(tmp_unit),
     >       sizeof(zm1_copy), nid, 2)
         call c_memwrite(ptr_vx_copy, sizeof(tmp_unit),
     >       sizeof(vx_copy), nid, 2)
         call c_memwrite(ptr_vy_copy, sizeof(tmp_unit),
     >       sizeof(vy_copy), nid, 2)
         call c_memwrite(ptr_vz_copy, sizeof(tmp_unit),
     >       sizeof(vz_copy), nid, 2)
         call c_memwrite(ptr_pr_copy, sizeof(tmp_unit),
     >       sizeof(pr_copy), nid, 2)

c-------------------------------------------------------------------------------
c         call check_ioinfo  
c         call set_outfld
c         call userchk     !user self-define subroutine
c         call prepost (ifoutfld,'his')
c         call in_situ_check()
         if (lastep .eq. 1) goto 1001

      enddo
c--ychuang
c      ntot2  = nx2*ny2*nz2*nelv
c      call copy_dual(pr,pr_even,ntot2)

 1001 lastep=1


      call nek_comm_settings(isyc,0)

      call comment

c     check for post-processing mode
      if (instep.eq.0) then
         nsteps=0
         istep=0
         if(nio.eq.0) write(6,*) 'call userchk'
         call userchk
         if(nio.eq.0) write(6,*) 'done :: userchk'
         call prepost (.true.,'his')
      else
         if (nio.eq.0) write(6,'(/,A,/)') 
     $      'end of time-step loop' 
      endif


      RETURN
      END

c-----------------------------------------------------------------------
      subroutine nek_advance(pr_new, pr_old)

      include 'SIZE'
      include 'TOTAL'
      include 'CTIMER'

      common /cgeom/ igeom
c--ychuang
c      real pr_new(lx2,ly2,lz2,lelv), pr_old(lx2,ly2,lz2,lelv)

      ntot = nx1*ny1*nz1*nelv

      call nekgsync

      call setup_convect(2) ! Save conv vel

      if (iftran) call settime
      if (ifmhd ) call cfl_check
      call setsolv
      call comment

#ifdef CMTNEK
      if (nio.eq.0.and.istep.le.1) write(6,*) 'CMT branch active'
      call cmt_nek_advance
      return
#endif


      if (ifsplit) then   ! PN/PN formulation


         do igeom=1,ngeom


         ! within cvode we use the lagged wx for 
         ! extrapolation, that's why we have to call it before gengeom 
         if (ifheat .and. ifcvode) call heat_cvode (igeom)   

         if (ifgeom) then
            call gengeom (igeom)
            call geneig  (igeom)
         endif

         if (ifheat) call heat (igeom)

         if (igeom.eq.2) then  
            call setprop
            call rzero(qtl,ntot)
            if (iflomach) call qthermal
         endif
c--ychuang
         if (ifflow)          call fluid    (pr_new, pr_old, igeom)
         if (ifmvbd)          call meshv    (igeom)
         if (param(103).gt.0) call q_filter (param(103))
         enddo

      else                ! PN-2/PN-2 formulation

         call setprop
         do igeom=1,ngeom
c--ychuang
c          print *, "igeom", igeom
c          ngeom=2
            if (igeom.gt.2) call userchk_set_xfer

            if (ifgeom) then
c--ychuang
c              ifgeom = F
               call gengeom (igeom)
               call geneig  (igeom)
            endif
c--ychuang
c            print *, "ifneknekm", ifneknekm
c               ifneknekm = F
            if (ifneknekm.and.igeom.eq.2) call multimesh_create
c--ychuang
c            print *, "ifmhd", ifmhd
c               ifmhd = F
c             print *, "ifpert", ifpert  F
c             print *, "ifbase", ifbase  T
c             print *, "ifheat", ifheat  F
c             print *, "ifflow", ifflow  T
c             print *, "ifmvbd", ifmvbd  F
            
            if (ifmhd) then
               if (ifheat)      call heat     (igeom)
                                call induct   (igeom)
            elseif (ifpert) then
               if (ifbase.and.ifheat)  call heat          (igeom)
               if (ifbase.and.ifflow)  call fluid(pr_new,pr_old,igeom)
               if (ifflow)             call fluidp        (igeom)
               if (ifheat)             call heatp         (igeom)
            else  ! std. nek case
c--ychuang
               if (ifheat)             call heat          (igeom)
               if (ifflow)             call fluid(pr_new,pr_old,igeom)
               if (ifmvbd)             call meshv         (igeom)
            endif

            if (igeom.eq.ngeom.and.param(103).gt.0) 
     $          call q_filter(param(103))
         enddo
      endif

      return
      end

c-----------------------------------------------------------------------
      subroutine nek_end

      include 'SIZE'
      include 'TSTEP'
      include 'PARALLEL'
      include 'OPCTR'

      if(instep.ne.0)  call runstat
      if(xxth(1).gt.0) call crs_stats(xxth(1))
      
      
   
      call in_situ_end()
      return
      end
c-----------------------------------------------------------------------
      subroutine nek__multi_advance(pr_new,pr_old,kstep,msteps)

      include 'SIZE'
      include 'TOTAL'
c--ychuang
c      real pr_new(lx2,ly2,lz2,lelv), pr_old(lx2,ly2,lz2,lelv)

      do i=1,msteps
         istep = istep+i
         call nek_advance(pr_new,pr_old)
c--ychuang
c         print *, "ifneknek", ifneknek
c-- FALSE
         if (ifneknek) call userchk_set_xfer
         if (ifneknek) call bcopy
         if (ifneknek) call chk_outflow

      enddo

      return
      end
c-----------------------------------------------------------------------
