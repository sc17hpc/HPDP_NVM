#include <linux/module.h>
#include <linux/kernel.h>
#include <asm/uaccess.h>
#include <linux/cdev.h>
#include <linux/proc_fs.h>

#include <linux/seq_file.h>
#include <linux/init.h>
#include <linux/version.h>
#include <linux/moduleparam.h>
#include <linux/major.h>
#include <linux/blkdev.h>
#include <linux/bio.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/time.h>
#include <asm/timer.h>
#include <linux/cpufreq.h>
#include <linux/crc32.h>
#include <linux/string.h>
#include <linux/ctype.h>
#include <linux/kthread.h>
#include <linux/sort.h>
#include <linux/timex.h>
#include <asm/tlbflush.h>
#include <asm/i387.h>


#define MAX_PROC_SIZE 100
static char proc_data[MAX_PROC_SIZE];

static struct proc_dir_entry *proc_write_entry;

int read_proc(char *buf,char **start,off_t offset,int count,int *eof,void *data )
{
  int len=0;
//  len = sprintf(buf,"%s\n ",proc_data);
//  printk("Read proc\n");
//  asm volatile ("wbinvd");
  wbinvd_on_all_cpus();
  return len;
}
/*
int write_proc(struct file *file,const char *buf,int count,void *data )
{

if(count > MAX_PROC_SIZE)
    count = MAX_PROC_SIZE;
if(copy_from_user(proc_data, buf, count))
    return -EFAULT;

return count;
}
*/
void create_new_proc_entry()
{
  proc_write_entry = create_proc_entry("wbinvd",0666,NULL);
  if(!proc_write_entry){
      printk(KERN_INFO "Error creating proc entry");
      return -ENOMEM;
  }
  proc_write_entry->read_proc = read_proc ;
//  proc_write_entry->write_proc = write_proc;
  printk(KERN_INFO "wbinvd initialized");

}

int proc_init (void) {
    create_new_proc_entry();
    return 0;
}

void proc_cleanup(void) {
    printk(KERN_INFO " Inside cleanup_module\n");
    remove_proc_entry("wbinvd",NULL);
}
MODULE_LICENSE("GPL");   
module_init(proc_init);
module_exit(proc_cleanup);
 
