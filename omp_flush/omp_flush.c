#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <time.h>
#include <omp.h>
#include "cflush_yh.h"
static size_t MM = 5120*1024;

//SIZE of L3 Cache = 20480K 

int main() {
    int *a, *b;
    int i;
    uint64_t start, end;
    unsigned cycles_low, cycles_high, cycles_low1, cycles_high1;
 
    a = (int *) malloc(MM*sizeof(int));
    b = (int *) malloc(MM*sizeof(int));


    for(i = 0; i < MM; ++i){
        a[i] = i;
//        b[i] = MM-i;
    }

    asm volatile ("CPUID\n\t" "RDTSC\n\t" 
        "mov %%edx, %0\n\t"
        "mov %%eax, %1\n\t": "=r" (cycles_high), "=r" (cycles_low):: "%rax", "%rbx", "%rcx", "%rdx");

    #pragma omp parallel for
    for (i=0; i<MM; i++){
	if ((i%16) ==0)
             cflush_yh(&a[i]);
    }


    asm volatile("RDTSCP\n\t" 
        "mov %%edx, %0\n\t" 
        "mov %%eax, %1\n\t"
        "CPUID\n\t": "=r" (cycles_high1), "=r" (cycles_low1):: "%rax", "%rbx", "%rcx", "%rdx");
    
    start = ( ((uint64_t)cycles_high << 32) | cycles_low );
    end = ( ((uint64_t)cycles_high1 << 32) | cycles_low1 );
    printf("Flush time  = %"PRIu64 " clock cycles\n\n", (end- start));

    free(a);
    free(b);

    return 0;
  }
