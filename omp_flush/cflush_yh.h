#ifndef CFLUSH_YH_H
#define CFLUSH_YH_H
#include <stdint.h>

inline static void mfence(void) {
	asm __volatile__ ("mfence" ::: "memory");
}

inline void cflush_yh(volatile int *p) {
	asm volatile ("clflush (%0)" :: "r"(p));

}
/*
static __inline__ unsigned long long rdtsc(void){
	unsigned long long int x;
	__asm__ volatile (".byte 0x0f, 0x31" : "=A"(x));
	return x;

}
*/
//inline static void clock_begin(unsigned cycles_low, unsigned cycles_high)
/*
void flush_range(double *arr, int length) {
	int *p = arr;
	int count;
	for(count =0;count<length; count ++){
		cflush_yh(p);
		p += 16;
		count += 1;
	}
}

*/
#endif
