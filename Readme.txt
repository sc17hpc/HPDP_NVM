NPB3.3-MPI
    In-place versioning of NPB benchmarks.
Nek5000
	In-place versioning of Nek5000 benchmarks
omp_flush
	Codes to measure clflush time with openmp
wbinvd
	Linux kernel module to implement WBINVD instruction